import styles from './Button.css';
import { Link } from 'react-router';
import React from 'react';

export default class Button extends React.Component {
  render() {
    return (
      <button type="button" className={styles.base}>
        {this.props.children}
      </button>
    );
  }
}
